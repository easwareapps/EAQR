import qrtools
import sys
if not len(sys.argv) == 2:
	print "syntax error: decode <filename>"
	exit()
qr=qrtools.QR()
if qr.decode(sys.argv[1]):
	print sys.argv[1] + "\t" + qr.data
