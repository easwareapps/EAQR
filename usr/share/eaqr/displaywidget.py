from PyQt4 import QtGui, QtCore

class displaywidget:
    def init(self):
        self.color1 = "#007FFF"
        self.color2 = "#DE801C"
        self.jpg_bg_color = "#FFFFFF"
        self.color_chooser = None
        widget = QtGui.QWidget()
        main_hbox = QtGui.QHBoxLayout()
        vbox = QtGui.QVBoxLayout()
        self.qr_image = QtGui.QLabel()
        self.qr_color_image = QtGui.QLabel()
        
        self.qr_image.setFixedSize(150, 150)
        self.qr_color_image.setFixedSize(150, 150)
        
        self.chk_gradiant = QtGui.QCheckBox("Enable Gradiant")
        self.chk_gradiant.setChecked(True)
        
        self.gradiant_one = QtGui.QPushButton("")
        self.gradiant_two = QtGui.QPushButton("")
        self.generate = QtGui.QPushButton("Generate")
        self.save_button = QtGui.QPushButton("Save QR Code")
        #self.generate.clicked.connect(self.generateQR)
        
        self.gradiant_one.setFixedSize(70, 70)
        self.gradiant_two.setFixedSize(70, 70)
        
        self.gradiant_one.setStyleSheet("QPushButton { background-color : #000FFF; }");
        self.gradiant_two.setStyleSheet("QPushButton { background-color : #DE801C; }");
        
        
        hbox = QtGui.QHBoxLayout()
        hbox.setAlignment(QtCore.Qt.AlignLeft)
        hbox.addWidget(self.gradiant_one)
        hbox.addWidget(self.gradiant_two)
        
        vbox.addWidget(self.chk_gradiant)
        vbox.addWidget(QtGui.QLabel("Qr Color"))
        vbox.addLayout(hbox)
        vbox.addWidget(self.qr_image)
        vbox.addWidget(self.qr_color_image)
        vbox.addWidget(self.generate)
        vbox.addWidget(self.save_button)
        self.gradiant_one.clicked.connect(self.change_color_1)
        self.gradiant_two.clicked.connect(self.change_color_2)
        QtCore.QObject.connect(self.chk_gradiant, QtCore.SIGNAL("toggled(bool)"), self.enable_gradiant)
        
        vbox.setAlignment(QtCore.Qt.AlignTop)
        main_hbox.addLayout(vbox)
        main_hbox.addLayout(self.get_save_options())
        widget.setLayout(main_hbox)
        return widget

    def enable_gradiant(self):
        if self.chk_gradiant.isChecked():
            self.gradiant_two.setVisible(True)
        else:
            self.gradiant_two.setVisible(False)
            
    def get_save_options(self):
        vbox = QtGui.QVBoxLayout()
        vbox.setAlignment(QtCore.Qt.AlignTop)
        self.save_black_qr = QtGui.QCheckBox("Save Black QR")
        self.save_color_qr = QtGui.QCheckBox("Save Color QR")
        
        
        self.save_png = QtGui.QCheckBox("Save PNG")
        self.save_svg = QtGui.QCheckBox("Save SVG")
        self.save_jpg = QtGui.QCheckBox("Save JPG")
        self.btn_jpg_background = QtGui.QPushButton("jpg background")
        self.btn_jpg_background.setStyleSheet("QPushButton {background-color:" + self.jpg_bg_color + "}")
        self.btn_jpg_background.setFixedSize(150, 30)
        self.label_msg = QtGui.QLabel("")
        self.label_msg.setFixedSize(150, 100)
        self.save_black_qr.setChecked(True)
        self.save_color_qr.setChecked(True)
        
        self.save_png.setChecked(True)
        self.save_svg.setChecked(True)
        self.save_jpg.setChecked(True)
        
        vbox.addWidget(self.save_black_qr)
        vbox.addWidget(self.save_color_qr)
        vbox.addWidget(self.save_png)
        vbox.addWidget(self.save_svg)
        vbox.addWidget(self.save_jpg)
        vbox.addWidget(self.btn_jpg_background)
        vbox.addWidget(self.label_msg)
        self.btn_jpg_background.clicked.connect(self.change_jpg_bg_color)
        return vbox
    
    def change_jpg_bg_color(self):
        self.button_type = 0
        self.select_color()
    def change_color_1(self):
        self.button_type = 1
        self.select_color()
    
    def change_color_2(self):
        self.button_type = 2
        self.select_color()
    
    def select_color(self):
        if self.color_chooser == None:
            self.color_chooser = QtGui.QColorDialog()
            QtCore.QObject.connect(self.color_chooser, QtCore.SIGNAL('colorSelected(QColor)'), self.change_color)
        self.color_chooser.show()
    
    def change_color(self, color):
        if self.button_type == 0:
            self.jpg_bg_color = color.name()
            self.btn_jpg_background.setStyleSheet("QPushButton { background-color : " + color.name() + " }");
        elif self.button_type == 1:
            self.color1 = color.name()
            self.gradiant_one.setStyleSheet("QPushButton { background-color : " + color.name() + " }");
        elif self.button_type == 2:
            self.color2 = color.name()
            self.gradiant_two.setStyleSheet("QPushButton { background-color : " + color.name() + " }");
    
    def get_color(self):
        return (self.color1, self.color2)
        
    def load_image(self, black, color):
        
        
        image = QtGui.QImage(black)
        pixmap = QtGui.QPixmap.fromImage(image)
        pixmap = pixmap.scaled(QtCore.QSize(150, 150), QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation);
        self.qr_image.setPixmap(pixmap)
        
        image = QtGui.QImage(color)
        pixmap = QtGui.QPixmap.fromImage(image)
        pixmap = pixmap.scaled(QtCore.QSize(150, 150), QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation);
        self.qr_color_image.setPixmap(pixmap)
        
        
        #self.scrollArea.setWidgetResizable(True)
        #lbl.show()
