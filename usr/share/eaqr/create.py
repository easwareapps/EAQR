from PIL import Image
import sys
import qrcode
import qrcode.image.svg
import cairosvg
import sys

if not len(sys.argv) == 2:
	print "syntax error: create <data>"
	exit()
	
factory = qrcode.image.svg.SvgPathImage
img = qrcode.make(sys.argv[1], image_factory=factory)
img.save("qr.svg")
exit()
data = open("qr.svg", "r").read()

'''convert svg to png'''
fout = open('/tmp/1.png','w')
cairosvg.svg2png(bytestring=data, write_to=fout)
fout.close()
'''color changer'''
img = Image.open("/tmp/1.png")
img = img.convert("RGBA")
pixdata = img.load()

# Clean the background noise, if color != white, then set to black.
# change with your color
for y in xrange(img.size[1]):
    for x in xrange(img.size[0]):
        if pixdata[x, y] == (0, 0, 0, 255):
            pixdata[x, y] = (0, 0, 255, 255)
        #else:
        #    pixdata[x, y] = (255, 255, 255, 255)

img.save("/tmp/img2.png", "PNG")


'''
import Image
import numpy as np

orig_color = (0,0,0)
replacement_color = (0,0,255)
img = Image.open("/tmp/1.png").convert('RGB')
data = np.array(img)
data[(data == orig_color).all(axis = -1)] = replacement_color
img2 = Image.fromarray(data, mode = 'RGB')
img2.show()

'''
