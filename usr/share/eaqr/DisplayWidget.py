from PyQt4 import QtGui, QtCore

class DisplayWidget:
    def init(self):
        widget = QtGui.QWidget()
        vbox = QtGui.QVBoxLayout()
        self.qr_image = QtGui.QLabel()
        self.qr_color_image = QtGui.QLabel()
        
        vbox.addWidget(self.qr_image)
        vbox.addWidget(self.qr_color_image)
        
        widget.setLayout(vbox)
        return widget
        
