from xml.dom import minidom
import math
import qrcode
import datetime
import time
import os
import qrcode.image.svg
import cairosvg
import cairo
import rsvg
from PIL import Image
import shutil

class QRFunctions:
    
    def __init__(self):
        self.qr_name = ""
        self.colored_qr_name = ""
        self.black_png = ""
        self.colored_png = ""
        self.black_jpg = ""
        self.colored_jpg = ""
        self.tmp_dir = os.tmpnam() + "/"
        os.mkdir(self.tmp_dir)
        
    def generateQR(self, data, color1, color2, bg_color): 
        
        n = datetime.datetime.now()
        unix_timestamp = int(time.mktime(n.timetuple()))
        
        self.qr_name = self.tmp_dir + "qr" + str(unix_timestamp) + ".svg"
        self.colored_qr_name = self.tmp_dir + "colored_qr" + str(unix_timestamp) + ".svg"
        
        factory = qrcode.image.svg.SvgPathImage
        svg = qrcode.make(data, image_factory=factory)
        svg.save(self.qr_name)
        
        size = self.resize_svg(self.qr_name)
        data = open(self.qr_name, "r").read()
        self.color_changer(data, self.colored_qr_name, color1, color2)
        self.black_png = self.svg2png(self.qr_name, size)
        self.colored_png = self.svg2png(self.colored_qr_name, size)
        self.black_jpg = self.png2jpg(self.black_png, bg_color)
        self.colored_jpg = self.png2jpg(self.colored_png, bg_color)
       
        
    def save_png_image(self, svg,  png):
        fout = open(png, "w")
        svg_code = open(svg, "r").read()
        cairosvg.svg2png(bytestring=svg_code,write_to=fout)
        fout.close()
        
    def resize_svg(self, svg_path):
        height = self.get_argument_values(svg_path, "svg", 'height')
        if height == "":
            height = "1mm"
        height = (float)(height.replace("mm", ""))
        
        multiply = math.ceil(512/height)
        size = int(multiply * height)
        
        viewbox = self.get_argument_values(svg_path, "svg", 'viewBox')
        viewbox = self.resize_numbers(viewbox, multiply)
        svg = '''<?xml version='1.0' encoding='UTF-8'?>
<svg height="''' + str(size) + '''px" version="1.1" viewBox="''' + viewbox + '''" width="''' + str(size) + '''px" xmlns="http://www.w3.org/2000/svg">
'''
        svg_code = self.get_argument_values(svg_path, "path", 'd')
        svg_code = self.resize_numbers(svg_code, multiply)
            
        svg_code = '''<path d="''' + svg_code + '''" id="qr-path" style="fill:#000000;fill-opacity:1;fill-rule:nonzero;stroke:none"/></svg>'''
        new_svg = open(svg_path, "w")
        new_svg.write(svg + svg_code)
        new_svg.close()
        return size
        
        
    def get_argument_values(self, svg, tag, arg):
        xmldoc = minidom.parse(svg)
        itemlist = xmldoc.getElementsByTagName(tag)
        val = ""
        for s in itemlist:
            val = s.attributes[arg].value
            #arg = self.resize_numbers(data, multiply)
        return val
        

    def resize_numbers(self, data, multiply):
        v=data.split(" ")
        for i in range(0, len(v)):
            try:
                no = int(v[i])
                v[i] = str(int(multiply) * int(no))
            except:
                pass
                
        data = ""
        for i in range(0, len(v)):
            data  += v[i]
            if not i == len(v)-1:
                data += " "
            
        return data
        
    def color_changer(self, data, name, color1, color2):
        #color1 = "#0007ff"
        #color2 = "#DE801C"
        prefix = '''xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   id="svg2"
   inkscape:version="0.91 r13725"
   sodipodi:docname="2.svg">
   <defs id="defs7">
   <linearGradient inkscape:collect="always" id="linearGradient4139">
    <stop style="stop-color:''' + color1 + ''';stop-opacity:1;" offset="0" id="stop4141" /> <stop style="stop-color:''' + color2 + ''';stop-opacity:1;" offset="1" id="stop4143" /></linearGradient></defs>'''
        data = data.replace("xmlns=\"http://www.w3.org/2000/svg\">", prefix)
        if color1 == color2:
            data = data.replace('''style="fill:#000000;''', '''style="fill:''' + color1 + ''';''')
        else:
            data = data.replace('''style="fill:#000000;''', '''style="fill:url(#linearGradient4139);''')
        newsvg = open(name, "w")
        newsvg.write(data)
        newsvg.close()
        pass
    
    def svg2png(self, svg, size):
        png = svg.replace(".svg", ".png")
        data = open(svg, "r").read()
        '''
        fout = open(png, 'w')
        cairosvg.svg2png(bytestring=data, write_to=fout)
        fout.close()'''
        img = cairo.ImageSurface(cairo.FORMAT_ARGB32, size,size)

        ctx = cairo.Context(img)

        handle= rsvg.Handle(None, str(data))

        handle.render_cairo(ctx)

        img.write_to_png(png)
        return png
    
    def png2jpg(self, png, bg_color):
        jpg = png.replace(".png", ".jpg")
        im = Image.open(png)
        bg = Image.new("RGB", im.size, self.hex_to_rgb(bg_color))
        bg.paste(im,im)
        bg.save(jpg)
        return jpg
    
    def hex_to_rgb(self, value):
        value = value.replace('#', "")
        lv = len(value)
        return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))
        
    def save_qr(self, path, black, color, png, svg, jpg, cli=False):
        path = str(path)
        if not path == "" and not self.qr_name == "" :
            if black:
                if svg:
                    file_path = path + "/black.svg"
                    if cli:
                        file_path = path
                    self.save_file(self.qr_name, file_path)
                if png:
                    file_path = path + "/black.png"
                    if cli:
                        file_path = path
                    self.save_file(self.black_png, file_path)
                if jpg:
                    file_path = path + "/black.jpg"
                    if cli:
                        file_path = path
                    self.save_file(self.black_jpg, file_path)
            if color:
                if svg:
                    file_path = path + "/color.svg"
                    if cli:
                        file_path = path
                    self.save_file(self.colored_qr_name, file_path)
                if png:
                    file_path = path + "/color.png"
                    if cli:
                        file_path = path
                    self.save_file(self.colored_png, file_path)
                if jpg:
                    file_path = path + "/color.jpg"
                    if cli:
                        file_path = path
                    self.save_file(self.colored_jpg, file_path)
            
                
        #if path.lower().endswith(".png"):
        #        self.save_file(self.colored_png, path)
        #    elif path.lower().endswith(".svg"):
        #        self.save_file(self.colored_qr_name, path)
        #    elif path.lower().endswith(".jpg"):
        #        self.save_file(self.colored_jpg, path)
                
            #self.save_file(self.qr_name, path + "/black.svg")
            #self.save_file(self.colored_qr_name, path + "/colored.svg")
            #self.save_file(self.black_png, path + "/black.png")
            #self.save_file(self.colored_png, path + "/color.png")
            #self.save_file(self.black_jpg, path + "/black.jpg")
            #self.save_file(self.colored_jpg, path + "/color.jpg")
    
    
    def save_file(self, src, dst):
        dst = self.get_non_exsist_file_name(dst)
        shutil.copyfile(src, dst)

    def get_non_exsist_file_name(self, name):
        tmp = name
        no = 0
        while os.path.exists(tmp):
            splitted = name.split(".")
            if len(splitted) >=2 :
                splitted[len(splitted)-2] = splitted[len(splitted)-2] + str(no)
            else:
                splitted[len(splitted)-1] = str(no) + splitted[len(splitted)-1]
            tmp = '.'.join(str(x) for x in splitted)
            no += 1
        print "new name", tmp
        return tmp
